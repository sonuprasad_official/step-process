#Simple form persisting state and components with data
Note : Gulp and browserify is used for task automation purpose.
### Install npm dependencies

```bash
npm install
```

### Run gulp

```bash
gulp
```
Probably you will get some error regarding SASS so run the following commands,

```bash
npm install -S node-sass
npm uninstall gulp-sass
npm install -S gulp-sass
gulp
```



This will run the `default` gulp task defined in `gulp/tasks/default.js`, which does the following:
- Run 'watch', which has 2 task dependencies, `['setWatch', 'browserSync']`
- `setWatch` sets a variable that tells the browserify task whether or not to use watchify.
- `browserSync` has `build` as a task dependecy, so that all your assets will be processed before browserSync tries to serve them to you in the browser.
- `build` includes the following tasks: `['browserify', 'sass', 'markup']`
- Compile and move these files to `build/` folder, and be served on your localhost at port 3000.
- Run appropriate tasks on when source files change and refresh the browser.

NOTE : SASS is used and some ui modifications are required when multiple long options are displayed. So, I am working on it.
Will update as soon as it gets solved .