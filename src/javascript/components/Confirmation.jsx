/**
 * @jsx React.DOM
 */
var React = require('react')

var Confirmation = React.createClass({
  render: function() {
    return (
      <div>
        <h2>Confirm Registration</h2>
        <ul>
          <li><b>Name:</b> {this.props.fieldValues.name}</li>
          <li><b>Email:</b> {this.props.fieldValues.email}</li>
          <li><b>question:</b> {this.props.fieldValues.question1}</li>
          <li><b>question2:</b> {this.props.fieldValues.question2}</li>
          <li><b>question3:</b> {this.props.fieldValues.question3}</li>
          <li><b>question4:</b> {this.props.fieldValues.question4}</li>
          <li><b>question5:</b> {this.props.fieldValues.question5}</li>
          <li><b>question6:</b> {this.props.fieldValues.question6}</li>
          <li><b>question7:</b> {this.props.fieldValues.question7}</li>
        </ul>
        <ul className="form-fields">
          <li className="form-footer">
            <button className="btn -default pull-left" onClick={this.props.previousStep}>Back</button>
            <button className="btn -primary pull-right" onClick={this.props.submitRegistration}>Submit Registration</button>
          </li>
        </ul>
      </div>
    )
  }
})

module.exports = Confirmation
