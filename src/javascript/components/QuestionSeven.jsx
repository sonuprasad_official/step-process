/**
 * @jsx React.DOM
 */
var React                   = require('react')
var getRadioOrCheckboxValue = require('../lib/radiobox-value')

var QuestionSeven = React.createClass({

  renderOptions: function(type, name, value, index) {
    var isChecked = function() {
      if (type == 'radio') return value == this.props.fieldValues[name]

      return false
    }.bind(this)

    return (
      <label key={index}>
        <input type={type} name={name} value={value} defaultChecked={isChecked()} /> {value}
      </label>
    )
  },

  render: function() {
    return (
      <div>
        <h2>Question 7</h2>
        <ul className="form-fields">
          <li className="radio">
            <span className="label">Which statement describes the extent of your financial cushion to meet foreseeable and unforeseen expenses?</span>
            {['Borderline : I have some cash and insurance. But I may need to dip into my investments or borrow in an emergency', 'Adequate : I have liquid assets. I have adequate insurance and unforeseen expenses can be managed by dipping in my portfolio', 'More than adequate: I have more than enough liquid assets to meet all my needs. I have sufficient insurance cover', 'Inadequate : My reserves are insufficient at present to take care of any contingency'].map(this.renderOptions.bind(this, 'radio', 'question7'))}
          </li>

          <li className="form-footer">
            <button className="btn -default pull-left" onClick={this.props.previousStep}>Back</button>
            <button className="btn -primary pull-right" onClick={this.nextStep}>Save &amp; Continue</button>
          </li>
        </ul>
      </div>
    )
  },

  nextStep: function() {
    // Get values via querySelector
    var question7    = document.querySelector('input[name="question7"]:checked')

    var data = {
      question7    : getRadioOrCheckboxValue(question7),
    }

    this.props.saveValues(data)
    this.props.nextStep()
  }
})

module.exports = QuestionSeven
