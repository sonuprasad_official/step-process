/**
 * @jsx React.DOM
 */
var React                   = require('react')
var getRadioOrCheckboxValue = require('../lib/radiobox-value')

var QuestionTwo = React.createClass({

  renderOptions: function(type, name, value, index) {
    var isChecked = function() {
      if (type == 'radio') return value == this.props.fieldValues[name]

      return false
    }.bind(this)

    return (
      <label key={index}>
        <input type={type} name={name} value={value} defaultChecked={isChecked()} /> {value}
      </label>
    )
  },

  render: function() {
    return (
      <div>
        <h2>Question 2</h2>
        <ul className="form-fields">
          <li className="radio">
            <span className="label">Please select your income</span>
            {['0 to 10 lakh', '10 to 30 lakh', 'More than 30 lakh'].map(this.renderOptions.bind(this, 'radio', 'question2'))}
          </li>

          <li className="form-footer">
            <button className="btn -default pull-left" onClick={this.props.previousStep}>Back</button>
            <button className="btn -primary pull-right" onClick={this.nextStep}>Save &amp; Continue</button>
          </li>
        </ul>
      </div>
    )
  },

  nextStep: function() {
    // Get values via querySelector
    var question2    = document.querySelector('input[name="question2"]:checked')

    var data = {
      question2    : getRadioOrCheckboxValue(question2),
    }

    this.props.saveValues(data)
    this.props.nextStep()
  }
})

module.exports = QuestionTwo
