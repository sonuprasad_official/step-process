/**
 * @jsx React.DOM
 */
var React                   = require('react')
var getRadioOrCheckboxValue = require('../lib/radiobox-value')

var QuestionSix = React.createClass({

  renderOptions: function(type, name, value, index) {
    var isChecked = function() {
      if (type == 'radio') return value == this.props.fieldValues[name]

      return false
    }.bind(this)

    return (
      <label key={index}>
        <input type={type} name={name} value={value} defaultChecked={isChecked()} /> {value}
      </label>
    )
  },

  render: function() {
    return (
      <div>
        <h2>Question 6</h2>
        <ul className="form-fields">
          <li className="radio">
            <span className="label">How would you react if your long term investment portfolio comes down by 30 percent due to market downturn?</span>
            {['You fear further downside and hence cut your losses and transfer your funds into more secure investments', 'You were ready for this and so you would leave the investments in place, expecting performance to improve', 'You would be concerned, but would wait to see if the investments improve', 'You would invest more funds to lower your average investment price, expecting growth over a longer term'].map(this.renderOptions.bind(this, 'radio', 'question6'))}
          </li>

          <li className="form-footer">
            <button className="btn -default pull-left" onClick={this.props.previousStep}>Back</button>
            <button className="btn -primary pull-right" onClick={this.nextStep}>Save &amp; Continue</button>
          </li>
        </ul>
      </div>
    )
  },

  nextStep: function() {
    // Get values via querySelector
    var question6    = document.querySelector('input[name="question6"]:checked')

    var data = {
      question6    : getRadioOrCheckboxValue(question6),
    }

    this.props.saveValues(data)
    this.props.nextStep()
  }
})

module.exports = QuestionSix
