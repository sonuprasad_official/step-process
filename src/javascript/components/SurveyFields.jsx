/**
 * @jsx React.DOM
 */
var React                   = require('react')
var getRadioOrCheckboxValue = require('../lib/radiobox-value')

var SurveyFields = React.createClass({

  renderOptions: function(type, name, value, index) {
    var isChecked = function() {
      if (type == 'radio') return value == this.props.fieldValues[name]

      return false
    }.bind(this)

    return (
      <label key={index}>
        <input type={type} name={name} value={value} defaultChecked={isChecked()} /> {value}
      </label>
    )
  },

  render: function() {
    return (
      <div>
        <h2>Question 1</h2>
        <ul className="form-fields">
          <li className="radio">
            <span className="label">What is your age ?</span>
            {['21 - 30', '30 - 45', '< 45'].map(this.renderOptions.bind(this, 'radio', 'question1'))}
          </li>

          <li className="form-footer">
            <button className="btn -default pull-left" onClick={this.props.previousStep}>Back</button>
            <button className="btn -primary pull-right" onClick={this.nextStep}>Save &amp; Continue</button>
          </li>
        </ul>
      </div>
    )
  },

  nextStep: function() {
    // Get values via querySelector
    var question1    = document.querySelector('input[name="question1"]:checked')

    var data = {
      question1    : getRadioOrCheckboxValue(question1),
    }

    this.props.saveValues(data)
    this.props.nextStep()
  }
})

module.exports = SurveyFields
