/**
 * @jsx React.DOM
 */
var React                   = require('react')
var getRadioOrCheckboxValue = require('../lib/radiobox-value')

var QuestionFive = React.createClass({

  renderOptions: function(type, name, value, index) {
    var isChecked = function() {
      if (type == 'radio') return value == this.props.fieldValues[name]

      return false
    }.bind(this)

    return (
      <label key={index}>
        <input type={type} name={name} value={value} defaultChecked={isChecked()} /> {value}
      </label>
    )
  },

  render: function() {
    return (
      <div>
        <h2>Question 5</h2>
        <ul className="form-fields">
          <li className="radio">
            <span className="label">Which statement best describes your earnings stream over the next few years?</span>
            {['Income stream is likely to fluctuate', 'Expect stable income', 'Expect a steady increase in income', 'Most probably it will go down'].map(this.renderOptions.bind(this, 'radio', 'question5'))}
          </li>

          <li className="form-footer">
            <button className="btn -default pull-left" onClick={this.props.previousStep}>Back</button>
            <button className="btn -primary pull-right" onClick={this.nextStep}>Save &amp; Continue</button>
          </li>
        </ul>
      </div>
    )
  },

  nextStep: function() {
    // Get values via querySelector
    var question5    = document.querySelector('input[name="question5"]:checked')

    var data = {
      question5    : getRadioOrCheckboxValue(question5),
    }

    this.props.saveValues(data)
    this.props.nextStep()
  }
})

module.exports = QuestionFive
