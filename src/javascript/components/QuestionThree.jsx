/**
 * @jsx React.DOM
 */
var React                   = require('react')
var getRadioOrCheckboxValue = require('../lib/radiobox-value')

var QuestionThree = React.createClass({

  renderOptions: function(type, name, value, index) {
    var isChecked = function() {
      if (type == 'radio') return value == this.props.fieldValues[name]

      return false
    }.bind(this)

    return (
      <label key={index}>
        <input type={type} name={name} value={value} defaultChecked={isChecked()} /> {value}
      </label>
    )
  },

  render: function() {
    return (
      <div>
        <h2>Question 3</h2>
        <ul className="form-fields">
          <li className="radio">
            <span className="label">How would you rate your knowledge of Investments i.e. Stocks, Mutual Funds, Bonds etc</span>
            {['Not familiar', 'Basic Knowledge', 'Fair Knowledge', 'Expert'].map(this.renderOptions.bind(this, 'radio', 'question3'))}
          </li>

          <li className="form-footer">
            <button className="btn -default pull-left" onClick={this.props.previousStep}>Back</button>
            <button className="btn -primary pull-right" onClick={this.nextStep}>Save &amp; Continue</button>
          </li>
        </ul>
      </div>
    )
  },

  nextStep: function() {
    // Get values via querySelector
    var question3    = document.querySelector('input[name="question3"]:checked')

    var data = {
      question3    : getRadioOrCheckboxValue(question3),
    }

    this.props.saveValues(data)
    this.props.nextStep()
  }
})

module.exports = QuestionThree
