/**
 * @jsx React.DOM
 */
var React                   = require('react')
var getRadioOrCheckboxValue = require('../lib/radiobox-value')

var QuestionFour = React.createClass({

  renderOptions: function(type, name, value, index) {
    var isChecked = function() {
      if (type == 'radio') return value == this.props.fieldValues[name]

      return false
    }.bind(this)

    return (
      <label key={index}>
        <input type={type} name={name} value={value} defaultChecked={isChecked()} /> {value}
      </label>
    )
  },

  render: function() {
    return (
      <div>
        <h2>Question 4</h2>
        <ul className="form-fields">
          <li className="radio">
            <span className="label">Which scenario appeals the most to you?</span>
            {['Gain 7% with no chance of loss', 'Gain 15% with a chance to lose 8%', 'Gain 20% with a chance to lose 15%', 'Gain 40% with a chance to lose 25%'].map(this.renderOptions.bind(this, 'radio', 'question4'))}
          </li>

          <li className="form-footer">
            <button className="btn -default pull-left" onClick={this.props.previousStep}>Back</button>
            <button className="btn -primary pull-right" onClick={this.nextStep}>Save &amp; Continue</button>
          </li>
        </ul>
      </div>
    )
  },

  nextStep: function() {
    // Get values via querySelector
    var question4    = document.querySelector('input[name="question4"]:checked')

    var data = {
      question4    : getRadioOrCheckboxValue(question4),
    }

    this.props.saveValues(data)
    this.props.nextStep()
  }
})

module.exports = QuestionFour
